﻿using OpenQA.Selenium;

namespace AutomacaoWeb.Helpers
{
    public class DriverFactory
    {
        public static IWebDriver INSTANCE { get; set; } = null;

        public static void CreateInstance()
        {

            INSTANCE = Browsers.GetLocalChrome();

        }

        public static void QuitInstace()
        {
            INSTANCE.Quit();
            INSTANCE.Dispose();
            INSTANCE = null;
        }
    }
}
