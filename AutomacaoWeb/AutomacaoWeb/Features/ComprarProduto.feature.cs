﻿// ------------------------------------------------------------------------------
//  <auto-generated>
//      This code was generated by SpecFlow (http://www.specflow.org/).
//      SpecFlow Version:2.4.0.0
//      SpecFlow Generator Version:2.4.0.0
// 
//      Changes to this file may cause incorrect behavior and will be lost if
//      the code is regenerated.
//  </auto-generated>
// ------------------------------------------------------------------------------
#region Designer generated code
#pragma warning disable
namespace AutomacaoWeb.Features
{
    using TechTalk.SpecFlow;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("TechTalk.SpecFlow", "2.4.0.0")]
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [NUnit.Framework.TestFixtureAttribute()]
    [NUnit.Framework.DescriptionAttribute("ComprarProduto")]
    public partial class ComprarProdutoFeature
    {
        
        private TechTalk.SpecFlow.ITestRunner testRunner;
        
#line 1 "ComprarProduto.feature"
#line hidden
        
        [NUnit.Framework.OneTimeSetUpAttribute()]
        public virtual void FeatureSetup()
        {
            testRunner = TechTalk.SpecFlow.TestRunnerManager.GetTestRunner();
            TechTalk.SpecFlow.FeatureInfo featureInfo = new TechTalk.SpecFlow.FeatureInfo(new System.Globalization.CultureInfo("en-US"), "ComprarProduto", "\tEu como internalta gostaria de inserir um produto no carrinho no site da SubMari" +
                    "no", ProgrammingLanguage.CSharp, ((string[])(null)));
            testRunner.OnFeatureStart(featureInfo);
        }
        
        [NUnit.Framework.OneTimeTearDownAttribute()]
        public virtual void FeatureTearDown()
        {
            testRunner.OnFeatureEnd();
            testRunner = null;
        }
        
        [NUnit.Framework.SetUpAttribute()]
        public virtual void TestInitialize()
        {
        }
        
        [NUnit.Framework.TearDownAttribute()]
        public virtual void ScenarioTearDown()
        {
            testRunner.OnScenarioEnd();
        }
        
        public virtual void ScenarioInitialize(TechTalk.SpecFlow.ScenarioInfo scenarioInfo)
        {
            testRunner.OnScenarioInitialize(scenarioInfo);
            testRunner.ScenarioContext.ScenarioContainer.RegisterInstanceAs<NUnit.Framework.TestContext>(NUnit.Framework.TestContext.CurrentContext);
        }
        
        public virtual void ScenarioStart()
        {
            testRunner.OnScenarioStart();
        }
        
        public virtual void ScenarioCleanup()
        {
            testRunner.CollectScenarioErrors();
        }
        
        public virtual void FeatureBackground()
        {
#line 4
 #line 5
 testRunner.Given("acesso o site do SubMarino", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Adicionar um produto no carrinho")]
        [NUnit.Framework.CategoryAttribute("Cenario")]
        [NUnit.Framework.CategoryAttribute("de")]
        [NUnit.Framework.CategoryAttribute("sucesso")]
        public virtual void AdicionarUmProdutoNoCarrinho()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Adicionar um produto no carrinho", null, new string[] {
                        "Cenario",
                        "de",
                        "sucesso"});
#line 8
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 4
 this.FeatureBackground();
#line 9
 testRunner.Given("que eu pesquise um produto desejado \"produto\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 10
 testRunner.And("o produto foi encontrado", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 11
 testRunner.And("ao encontrar desenho inseri-lo no carrinho", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 12
 testRunner.When("eu escoher um dos produtos pesquisado", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 13
 testRunner.Then("o produto irá para o carrinho para efetuar a compra", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Adicionar um produto inexistente")]
        [NUnit.Framework.CategoryAttribute("Cenario")]
        [NUnit.Framework.CategoryAttribute("falha")]
        [NUnit.Framework.CategoryAttribute("buscar")]
        [NUnit.Framework.CategoryAttribute("um")]
        [NUnit.Framework.CategoryAttribute("produto")]
        public virtual void AdicionarUmProdutoInexistente()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Adicionar um produto inexistente", null, new string[] {
                        "Cenario",
                        "falha",
                        "buscar",
                        "um",
                        "produto"});
#line 17
 this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 4
 this.FeatureBackground();
#line 18
 testRunner.Given("que eu pesquise um produto inexistente", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 19
 testRunner.Then("o sistema retorna que nao existe o produto", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Adicionar um produto indisponivel para venda")]
        [NUnit.Framework.CategoryAttribute("Cenario")]
        [NUnit.Framework.CategoryAttribute("escolhendo")]
        [NUnit.Framework.CategoryAttribute("produto")]
        [NUnit.Framework.CategoryAttribute("indisponivel")]
        public virtual void AdicionarUmProdutoIndisponivelParaVenda()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Adicionar um produto indisponivel para venda", null, new string[] {
                        "Cenario",
                        "escolhendo",
                        "produto",
                        "indisponivel"});
#line 22
 this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 4
 this.FeatureBackground();
#line 23
 testRunner.Given("que eu pesquise um produto desejado", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 24
 testRunner.And("o produto foi encontrado", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 25
 testRunner.When("eu escolho o produto desejado", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 26
 testRunner.Then("o sistema informa que o produto esta indisponivel", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
    }
}
#pragma warning restore
#endregion

