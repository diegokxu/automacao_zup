﻿Feature: ComprarProduto
	Eu como internalta gostaria de inserir um produto no carrinho no site da SubMarino

	Background: Usuario deverá está logado no site 
	Given acesso o site do SubMarino

@Cenario de sucesso
Scenario: Adicionar um produto no carrinho
	Given que eu pesquise um produto desejado "produto"
	And o produto foi encontrado
	And  ao encontrar desenho inseri-lo no carrinho
	When eu escoher um dos produtos pesquisado
	Then o produto irá para o carrinho para efetuar a compra


	@Cenario falha buscar um produto 
	Scenario: Adicionar um produto inexistente 
	Given que eu pesquise um produto inexistente
	Then o sistema retorna que nao existe o produto

	@Cenario escolhendo produto indisponivel
	Scenario: Adicionar um produto indisponivel para venda
	Given que eu pesquise um produto desejado
	And o produto foi encontrado
	When eu escolho o produto desejado
	Then o sistema informa que o produto esta indisponivel
	
