﻿using AutomacaoWeb.Helpers;
using AutomacaoWeb.Pages;
using NUnit.Framework;
using System;
using TechTalk.SpecFlow;

namespace AutomacaoWeb.StepsDefinitions
{
    [Binding]
    public class ComprarProdutoSteps
    {
        Random r = new Random();
        MainPage mainPage = new MainPage();
        ResultsPage resultsPage = new ResultsPage();
        ProductsPage productsPage = new ProductsPage();
        CarrinhoPage carrinhoPage = new CarrinhoPage();
        string[] produtos = { "PC Gamer", "Geladeira", "Celular" };
      
        
        [Given(@"que eu pesquise um produto desejado ""(.*)""")]
        public void GivenQueEuPesquiseUmProdutoDesejado(string p0)
        {
            p0 = produtos[r.Next(3)];
            mainPage.ProcurarProdutos(p0);
            mainPage.ClicarEmPesquisar();
        }


        [Given(@"que eu pesquise um produto desejado")]
        public void GivenQueEuPesquiseUmProdutoDesejado()
        {          
            
            mainPage.ProcurarProdutos("Teclado gamer");
            mainPage.ClicarEmPesquisar();
        }

        [Given(@"o produto foi encontrado")]
        public void GivenOProdutoFoiEncontrado()
        {
            int teste = resultsPage.RetornaParteProdutosEncontradoResultado();
            Assert.Greater(resultsPage.RetornaParteProdutosEncontradoResultado(), 0);
        }


        [Given(@"ao encontrar desenho inseri-lo no carrinho")]
        public void GivenAoEncontrarDesenhoInseri_LoNoCarrinho()
        {
            resultsPage.EscolherUmProduto();
        }
        
        [When(@"eu escoher um dos produtos pesquisado")]
        public void WhenEuEscoherUmDosProdutosPesquisado()
        {
            productsPage.Escolherproduto();
            productsPage.ContinuarCompra();
        }
        
        [Then(@"o produto irá para o carrinho para efetuar a compra")]
        public void ThenOProdutoIraParaOCarrinhoParaEfetuarACompra()
        {
            Assert.That(carrinhoPage.RetornaTituloPagina().Contains("Meu carrinho"));
            Assert.That(carrinhoPage.RetornarDetalhesProduto().Contains("1 produto"));
        }

        [Given(@"que eu pesquise um produto inexistente")]
        public void GivenQueEuPesquiseUmProdutoInexistente()
        {
            string produto = "zzads";
            mainPage.ProcurarProdutos(produto);
            mainPage.ClicarEmPesquisar();
        }

        [Then(@"o sistema retorna que nao existe o produto")]
        public void ThenOSistemaRetornaQueNaoExisteOProduto()
        {
            Assert.IsTrue(resultsPage.ProdutoNaoEncontrado());
        }

        [When(@"eu escolho o produto desejado")]
        public void WheneuEscolhoOProdutoDesejado()
        {
            resultsPage.EscolherProdutoIndisponivel();
        }


        [Then(@"o sistema informa que o produto esta indisponivel")]
        public void ThenOSistemaInformaQueOProdutoEstaIndisponivel()
        {
            Assert.AreEqual("Ops! Já vendemos o estoque", resultsPage.RetornaTextoResultadoEstoque());
        }



    }
}
