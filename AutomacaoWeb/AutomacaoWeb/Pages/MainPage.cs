﻿using AutomacaoWeb.Bases;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomacaoWeb.Pages
{
   public class MainPage : PageBase
    {
        By campoPesquisar = By.XPath("//input[@id='h_search-input']");
        By botaoPesquisar = By.Id("h_search-btn");


        public void ProcurarProdutos(String produto)
        {
            SendKeys(campoPesquisar, produto);
        }

        public void ClicarEmPesquisar()
        {
            Click(botaoPesquisar);
        }

    }
}
