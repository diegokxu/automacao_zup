﻿using AutomacaoWeb.Bases;
using OpenQA.Selenium;

namespace AutomacaoWeb.Pages
{
    public class CarrinhoPage : PageBase
    {
        By tituloPagina = By.XPath("//h2[@class='page-title']");
        By detalhesPedido = By.XPath("//li[@class='summary-detail']/span");


        public string RetornarDetalhesProduto()
        {
            return GetText(detalhesPedido);
        }

        public string RetornaTituloPagina()
        {
            return GetText(tituloPagina);
        }

    }
}
