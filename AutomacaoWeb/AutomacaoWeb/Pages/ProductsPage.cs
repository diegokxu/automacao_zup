﻿using AutomacaoWeb.Bases;
using OpenQA.Selenium;

namespace AutomacaoWeb.Pages
{
    public class ProductsPage : PageBase
    {
        By botaComprar = By.XPath("//div[@class='Wrapper-sc-1i9za0i-3 hyuQAM ViewUI-sc-1ijittn-6 iXIDWU']");
        By botaContinuar = By.XPath("//div[@class='Wrapper-sc-1i9za0i-3 ibqKJd ViewUI-sc-1ijittn-6 iXIDWU']");

        public void Escolherproduto()
        {
            Click(botaComprar);
        }

        public void ContinuarCompra()
        {
            Click(botaContinuar);
        }
    }
}
