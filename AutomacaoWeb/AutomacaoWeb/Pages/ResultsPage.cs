﻿using AutomacaoWeb.Bases;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;

namespace AutomacaoWeb.Pages
{
    public class ResultsPage : PageBase
    {
        By retornoProdutoNaoEncontrado = By.XPath("//span[starts-with(.,'Não encontramos nenhum')]");
        By textEstoqueEsgotado = By.Id("title-stock");


        public int RetornaParteProdutosEncontradoResultado() {
            return RetornaExistenciaDeProdutoNoResultado().Count;
        }

        public void EscolherUmProduto()
        {
            Random r = new Random();
            List<IWebElement> produtos = RetornaExistenciaDeProdutoNoResultado();
            string produtoAlatorio = produtos[r.Next(produtos.Count)-1].Text;            
            Click(By.XPath("//h2[starts-with(.,"+"'"+produtoAlatorio.Substring(0,31)+"'"+")]"));
        }

        public void EscolherProdutoIndisponivel()
        {
            List<IWebElement> produtos = RetornaExistenciaDeProdutoNoResultado();
           string produtoIndisponivel  = produtos.Find(x => x.Text.Contains("Já vendemos todo")).Text;           
            Click(By.XPath("//h2[contains(.," + "'" + produtoIndisponivel.Substring(0, 31) + "'" + ")]"));
            
        }


        public bool ProdutoNaoEncontrado()
        {
            return ReturnIfElementIsDisplayed(retornoProdutoNaoEncontrado);
        }

        public string RetornaTextoResultadoEstoque()
        {
            return GetText(textEstoqueEsgotado);
        }

    }
}
