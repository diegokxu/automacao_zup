﻿using AutomacaoWeb.Helpers;
using System;
using System.Diagnostics;
using System.Collections.Generic;
using OpenQA.Selenium;
using System.Configuration;


namespace AutomacaoWeb.Bases
{
    public class PageBase
    {

        protected OpenQA.Selenium.Support.UI.WebDriverWait wait { get; private set; }
        protected IWebDriver driver { get; private set; }
        protected IJavaScriptExecutor javaScriptExecutor { get; private set; }


        public PageBase()
        {
            wait = new OpenQA.Selenium.Support.UI.WebDriverWait(DriverFactory.INSTANCE, TimeSpan.FromSeconds(Convert.ToDouble(ConfigurationManager.AppSettings["timeout_default"].ToString())));
            driver = DriverFactory.INSTANCE;
            javaScriptExecutor = (IJavaScriptExecutor)driver;

        }


        private void WaitUntilPageReady()
        {
            Stopwatch timeOut = new Stopwatch();
            timeOut.Start();

            while (timeOut.Elapsed.Seconds <= Convert.ToInt32(ConfigurationManager.AppSettings["timeout_default"].ToString()))
            {
                string documentState = javaScriptExecutor.ExecuteScript("return document.readyState").ToString();
                if (documentState.Equals("complete"))
                {
                    timeOut.Stop();
                    break;
                }
            }
        }


        protected IWebElement WaitForElement(By locator)
        {
            WaitUntilPageReady();
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(locator));
            IWebElement element = driver.FindElement(locator);
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(element));
            return element;
        }

        protected IWebElement WaitForElementByTime(By locator, TimeSpan time)
        {
            WaitUntilPageReady();
            OpenQA.Selenium.Support.UI.WebDriverWait waitTime = new OpenQA.Selenium.Support.UI.WebDriverWait(driver, time);
            waitTime.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(locator));
            IWebElement element = driver.FindElement(locator);
            waitTime.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(element));
            return element;
        }


        protected void Click(By locator)
        {
            Stopwatch timeOut = new Stopwatch();
            timeOut.Start();

            while (timeOut.Elapsed.Seconds <= Convert.ToInt32(ConfigurationManager.AppSettings["timeout_default"]))
            {

                WaitForElement(locator).Click();
                timeOut.Stop();
                return;

            }
        }

        protected void SendKeys(By locator, string text)
        {
            WaitForElement(locator).SendKeys(text);
        }


        protected string GetText(By locator)
        {
            string text = WaitForElement(locator).Text;
            return text;
        }

        protected bool ReturnIfElementIsDisplayed(By locator)
        {
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(locator));
            bool result = driver.FindElement(locator).Displayed;
            return result;
        }

        protected List<IWebElement> RetornaExistenciaDeProdutoNoResultado()
        {
            var grid = driver.FindElement(By.XPath("//div[@class='row product-grid no-gutters main-grid']"));
            var divs = grid.FindElements(By.XPath("//div[@class='product-grid-item ColUI-gjy0oc-0 ifczFg ViewUI-sc-1ijittn-6 iXIDWU']"));
            List<IWebElement> produtos = new List<IWebElement>();
            foreach (var produtosEncontrados in divs)
            {
                produtos.Add(produtosEncontrados);
            }
            return produtos;
        }


        protected List<string> RetornaElemtosDaTabela(By locator)
        {
            IWebElement tabela = driver.FindElement(locator);
            IList<IWebElement> td = tabela.FindElements(By.XPath("//td[@class='task_body col-md-7 limit-word-size']"));
            List<string> valores = new List<string>();
            foreach (IWebElement tarefa in td)
            {
                valores.Add(tarefa.Text);
            }
            return valores;
        }

    }
}